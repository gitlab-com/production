
/label ~change ~"change::unscheduled" ~"group::global search" ~"Service::Elasticsearch"

<!--
Please review https://about.gitlab.com/handbook/engineering/infrastructure/change-management/ for the most recent information on our change plans and execution policies.
-->

# Production Change

### Change Summary

The Global Search Elasticsearch cluster `{{ .productionClusterName}}` will be upgraded to Elasticsearch version `{{ .version }}`. The staging cluster `{{ .stagingClusterName }}` will be upgraded first to verify.

### Change Details

<!--
To automatically add your change to the GitLab Production calendar update the following fields:
- Time tracking
- Scheduled Date and Time (UTC in format YYYY-MM-DD HH:MM)

Bot: https://gitlab.com/gitlab-com/gl-infra/ops-team/toolkit/change-scheduler
-->

1. **Services Impacted**  - ~"Service::Search" ~"Service::Elasticsearch"
1. **Change Technician**  - {+ DRI for the execution of this change +}
1. **Change Reviewer**    - {+ DRI for the review of this change +}
1. **Scheduled Date and Time (UTC in format YYYY-MM-DD HH:MM)** - {+ Start date and time planned to execute change steps YYYY-MM-DD HH:MM +}
1. **Time tracking**      - 120 minutes (changes) + 360 minutes (rollback, depending on how long indexing was paused)
1. **Downtime Component** - No downtime required for minor (rolling upgrade) or major (blue/green deployment) version upgrades. However, indexing will be paused during minor and major upgrades to prevent data loss. Pausing indexing means that updates made to database data will not be immediately available to the search service. Once the upgrade is complete, indexing will be unpaused.

## Detailed steps for the change

### Change Steps - steps to take to execute the change

*Estimated Time to Complete (mins)* - 4 hours

#### Phase 0: Pre-flight
- [ ] [Create Elasticsearch Upgrade issue](https://gitlab.com/gitlab-org/search-team/team-tasks/-/issues/new?issue[title]=Elasticsearch%20upgrade%20to%20VERSION&issuable_template=Elasticsearch-upgrade) and link to this issue
- [ ] Upgrade CI pipelines in gitlab and gdk
- [ ] Upgrade QA nightly builds
- [ ] Add label ~C2 for major version upgrades or ~C3 for minor version upgrades
- [ ] Ensure monitoring cluster is compatible with new version, per Elastic upgrade instructions: https://www.elastic.co/guide/en/elastic-stack/current/upgrading-elastic-stack.html
- [ ] Verify that there are no errors in the Staging or in the Production cluster and that both are healthy
- [ ] Verify that there are no alerts firing for the Advanced Search feature, Elasticsearch, Sidekiq workers, or redis
- [ ] Set label ~"change::in-progress" `/label ~change::in-progress`

#### Phase 1: Staging Upgrade

**Major/minor version upgrade: pause indexing**

- [ ] Pause indexing through the [Advanced Search Admin UI](https://staging.gitlab.com/admin/application_settings/advanced_search) or the console `::Gitlab::CurrentSettings.update!(elasticsearch_pause_indexing: true)`
- [ ] Wait 2 mins for queues in redis to drain and for [inflight jobs](https://dashboards.gitlab.net/d/sidekiq-shard-detail/sidekiq-shard-detail?orgId=1&from=now-30m&to=now&var-PROMETHEUS_DS=Global&var-environment=gstg&var-stage=main&var-shard=elasticsearch&viewPanel=11) to finish
- [ ] Add a new comment to an issue and verify that the [Elasticsearch queue increases in the graph](https://dashboards.gitlab.net/d/stage-groups-global_search/stage-groups-group-dashboard-enablement-global-search?orgId=1&var-PROMETHEUS_DS=Global&var-environment=gstg&var-stage=main&var-controller=All&var-action=All&from=now-5m&to=now)


**Major version upgrade: blue/green deploy**

- [ ] In the Elastic Cloud UI, create a snapshot
- [ ] Create new version deployment called `staging-gitlab-com indexing-<CURRENT_DATE>`
  - [ ] Tick the box `Restore snapshot data`
  - [ ] Select [+ staging +] deployment in the `Restore from` dropdown
  - [ ] Select new version in the `Version` dropdown
  - [ ] Ensure there is enough capacity for staging at least `120 GB storage | 4 GB RAM | Up to 2.5 vCPU` across two zones
  - [ ] Store username and password of new cluster in 1Password vault `Global Search`
  - [ ] Ensure the password gets moved to the [infrastructure 1Password staging vault](https://start.1password.com/open/i?a=LKATQYUATRBRDHRRABEBH4RJ5Y&v=7xbs54owvjux3cypztlhyetej4&i=bcyedfifhymhiavpyuifyrk65i&h=gitlab.1password.com). Will need SRE help
- [ ] Make local copy of current Advanced Search settings
  - [ ] Endpoint, username, password
- [ ] Ensure new cluster is added to Monitoring cluster   
- [ ] Change Advanced Search settings
  - [ ] `elasticsearch_url` endpoint to new staging endpoint
  - [ ] `elasticsearch_user` to new staging user
  - [ ] `elasticsearch_password` to new staging password
  - [ ] Save changes
- [ ] Test read code paths
  - [ ] Code
  - [ ] Notes
- [ ] Resume indexing and wait for queue to drain
- [ ] Wait until the [Sidekiq Queues (Global Search)](https://dashboards.gitlab.net/d/sidekiq-main/sidekiq-overview?orgId=1) have caught up
- [ ] Test write code paths
  - [ ] Code
  - [ ] Notes
- [ ] Schedule a future change to delete unused deployments after a week

**Minor version upgrades: rolling deployment**

- [ ] In the Elastic Cloud UI, create a snapshot
- [ ] In the Elastic Cloud UI, click Upgrade for the deployment 
- [ ] Select the version, click the Upgrade button
- [ ] Test read code paths
  - [ ] Code
  - [ ] Notes
- [ ] Test write code paths
  - [ ] Code
  - [ ] Notes
#### Phase 2: Production Upgrade

**Major/minor version upgrade: pause indexing**

- [ ] Add a silence via https://alerts.gitlab.net/#/silences/new with a matcher on the following alert names (link the comment field in each silence back to the Change Request Issue URL)
    - [ ] `alertname="SearchServiceElasticsearchIndexingTrafficAbsent"` ->
    - [ ] `alertname="gitlab_search_indexing_queue_backing_up"` ->
    - [ ] `alertname="SidekiqServiceGlobalSearchIndexingApdexSLOViolation"` ->
    - [ ] `alertname="SidekiqServiceGlobalSearchIndexingTrafficCessation"` ->
- [ ] Pause indexing in the [Advanced Search Admin UI](https://gitlab.com/admin/application_settings/advanced_search) or through the console `::Gitlab::CurrentSettings.update!(elasticsearch_pause_indexing: true)`
- [ ] Wait 2 mins for queues in redis to drain and for [inflight jobs](https://dashboards.gitlab.net/d/sidekiq-shard-detail/sidekiq-shard-detail?orgId=1&from=now-30m&to=now&var-PROMETHEUS_DS=Global&var-environment=gprd&var-stage=main&var-shard=elasticsearch&viewPanel=11) to finish
- [ ] Verify that the [Elasticsearch queue increases in the graph](https://dashboards.gitlab.net/d/stage-groups-global_search/stage-groups-group-dashboard-enablement-global-search?orgId=1&var-PROMETHEUS_DS=Global&var-environment=gprd&var-stage=main&var-controller=All&var-action=All&from=now-5m&to=now)

**Major version upgrades: blue/green deploy**

- [ ] In the Elastic Cloud UI, create snapshot
- [ ] Create a new version deployment called `prod-gitlab-com indexing-<CURRENT_DATE>`
  - [ ] Tick the box `Restore snapshot data`
  - [ ] Select [- production -] deployment in the `Restore from` dropdown
  - [ ] Select `{{ .version }}` in the `Version` dropdown
  - [ ] Ensure there is enough capacity for staging at least `13.13 TB storage | 448 GB RAM | 69 vCPU` across two zones
  - [ ] Store username and password of new cluster in 1Password vault `Global Search`
  - [ ] Store username and password of new cluster in [Infrastructure 1Password production vault](https://start.1password.com/open/i?a=LKATQYUATRBRDHRRABEBH4RJ5Y&v=7xbs54owvjux3cypztlhyetej4&i=hqe52ptetdxoukvn5dqfrp7jru&h=gitlab.1password.com). Will need SRE help
- [ ] Change Elastic password offline from Zoom
- [ ] Make local copy of current Advanced Search settings
  - [ ] Endpoint, username, password  
- [ ] Change Advanced Search settings `ApplicationSetting.current.update(elasticsearch_url: ELASTIC_URL, elasticsearch_username: ELASTIC_USER, elasticsearch_password: ELASTIC_PASSWORD)`
  - [ ] `elasticsearch_url` endpoint to production endpoint
  - [ ] `elasticsearch_user` to production user
  - [ ] `elasticsearch_password` to production password
  - [ ] Save changes
- [ ] Test read code paths
  - [ ] Code
  - [ ] Notes
- [ ] Resume indexing in the Advanced Search Admin UI or the rails console `Gitlab::CurrentSettings.update!(elasticsearch_pause_indexing: false)`
- [ ] Wait until the [Sidekiq Queues (Global Search)](https://dashboards.gitlab.net/d/sidekiq-main/sidekiq-overview?orgId=1) have caught up
- [ ] Test write code paths
  - [ ] Code
  - [ ] Notes
- [ ] Schedule a future change to delete unused deployments after a week

**Minor version upgrades: rolling deploy**

- [ ] In the Elastic Cloud UI, create snapshot
- [ ] In the Elastic Cloud UI, click Upgrade for the deployment 
- [ ] Select the version, click the Upgrade button
- [ ] Test read code paths
  - [ ] Code
  - [ ] Notes
- [ ] Test write code paths
  - [ ] Code
  - [ ] Notes

## Rollback

### Rollback steps - steps to be taken in the event of a need to rollback this change

*Estimated Time to Complete (mins)* - 30-120 minutes (depending on how long indexing was paused)

- [ ] [Major upgrade only] Change Advanced Search settings back to original cluster
- [ ] Resume indexing
- [ ] Set label ~"change::aborted" `/label ~change::aborted`
- [ ] [Major upgrade only] Delete unused deployments (or schedule a future change to delete them if needed for analysis)

## Monitoring

### Key metrics to observe

<!--
  * Describe which dashboards and which specific metrics we should be monitoring related to this change using the format below.
-->

#### sidekiq

- Metric: Search sidekiq indexing queues (Sidekiq Queues (Global Search))
  - Location: https://dashboards.gitlab.net/d/sidekiq-main/sidekiq-overview?orgId=1
  - What changes to this metric should prompt a rollback: Queues not draining
- Metric: Search sidekiq in flight jobs
  - Location: https://dashboards.gitlab.net/d/sidekiq-shard-detail/sidekiq-shard-detail?orgId=1&from=now-30m&to=now&var-PROMETHEUS_DS=Global&var-environment=gprd&var-stage=main&var-shard=elasticsearch
  - What changes to this metric should prompt a rollback: No jobs in flight
- Metric: sidekiq shard (elasticsearch) detail
    - Location: https://dashboards.gitlab.net/d/sidekiq-shard-detail/sidekiq-shard-detail?orgId=1&var-PROMETHEUS_DS=Global&var-environment=gprd&var-stage=main&var-shard=catchall&var-shard=elasticsearch
    - What changes to this metric should prompt a rollback: **After unpausing indexing:** Elevated queue length that does not resolve 
    
#### gitaly

- Metric: gitaly overview dashboard
  - Location: https://dashboards.gitlab.net/d/gitaly-main/gitaly-overview?orgId=1
  - What changes to this metric should prompt a rollback: **After unpausing indexing:** Degradation in gitaly service, we may want to only pause indexing again to let gitaly catch up (vs. a full rollback of the change)

#### PostGreSQL

- Metric: PostGreSQL overview dashboard
  - Location: https://dashboards.gitlab.net/d/000000144/postgresql-overview?orgId=1
  - What changes to this metric should prompt a rollback: **After unpausing indexing:** Degradation in PostGreSQL service, we may want to only pause indexing again to let PostGreSQL catch up (vs. a full rollback of the change)


#### Performance

- Metric: Search overview metrics
  - Location: https://dashboards.gitlab.net/d/search-main/search-overview?orgId=1
  - What changes to this metric should prompt a rollback: Flatline of RPS
- Metric: Search controller performance
  - Location: https://dashboards.gitlab.net/d/web-rails-controller/web-rails-controller?orgId=1&var-PROMETHEUS_DS=Global&var-environment=gprd&var-stage=main&var-controller=SearchController&var-action=show
  - What changes to this metric should prompt a rollback: Massive spike in latency

#### Elastic Cloud

- Metric: Elastic Cloud outages
  - Location: https://status.elastic.co/#past-incidents
  - What changes to this metric should prompt a rollback: Incidents which prevent upgrade of the cluster

## Change Reviewer checklist

<!--
To be filled out by the reviewer.
-->
~C4 ~C3 ~C2 ~C1:
- [ ] Check if the following applies:
  - The **scheduled day and time** of execution of the change is appropriate.
  - The [change plan](#detailed-steps-for-the-change) is technically accurate.
  - The change plan includes **estimated timing values** based on previous testing.
  - The change plan includes a viable [rollback plan](#rollback).
  - The specified [metrics/monitoring dashboards](#key-metrics-to-observe) provide sufficient visibility for the change.

~C2 ~C1:
- [ ] Check if the following applies:
  - The complexity of the plan is appropriate for the corresponding risk of the change. (i.e. the plan contains clear details).
  - The change plan includes success measures for all steps/milestones during the execution.
  - The change adequately minimizes risk within the environment/service.
  - The performance implications of executing the change are well-understood and documented.
  - The specified metrics/monitoring dashboards provide sufficient visibility for the change.
      - If not, is it possible (or necessary) to make changes to observability platforms for added visibility?
  - The change has a primary and secondary SRE with knowledge of the details available during the change window.
  - The labels ~"blocks deployments" and/or ~"blocks feature-flags" are applied as necessary

## Change Technician checklist

<!--
To find out who is on-call, in #production channel run: /chatops run oncall production.
-->

- [ ] Check if all items below are complete:
  - The [change plan](#detailed-steps-for-the-change) is technically accurate.
  - This Change Issue is linked to the appropriate Issue and/or Epic
  - Change has been tested in staging and results noted in a comment on this issue.
  - A dry-run has been conducted and results noted in a comment on this issue.
  - For ~C1 and ~C2 change issues, the SRE on-call has been informed prior to change being rolled out. (In #production channel, mention `@sre-oncall` and this issue and await their acknowledgement.)
  - Release managers have been informed (If needed! Cases include DB change) prior to change being rolled out. (In #production channel, mention `@release-managers` and this issue and await their acknowledgment.)
  - There are currently no [active incidents](https://gitlab.com/gitlab-com/gl-infra/production/-/issues?scope=all&utf8=%E2%9C%93&state=opened&label_name[]=Incident%3A%3AActive) that are ~severity::1 or ~severity::2
  - If the change involves doing maintenance on a database host, an appropriate silence targeting the host(s) should be added for the duration of the change.
