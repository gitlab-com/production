[[_TOC_]]

# Production Change - Criticality 2 ~C2

## Change Summary

This production issue will be used for Gamedays to simulate recovery in a zonal outage. The test simulates an outage without shutting down any CI servers to prevent service disruption. Instead, we’ll increase capacity in one of the shard colours to observe how the system manages load distribution. The goal is to ensure that if one zone goes down, the system can automatically balance the load and continue running smoothly after increased capacity.

The following steps will be followed when restoring CI Runners capacity in a single zone.

### Gameday execution roles and details

| Role | Assignee |
|------|----------|
| Change Technician | <!-- woodhouse: '`@{{ .Username }}`' --> |
| Change Reviewer |  <!-- woodhouse: '@{{ .Reviewer }}' --> |

- **Services Impacted** - ~"Service::CI Runners"
- **Time tracking**      - <!-- woodhouse: '{{ .Duration}}' --> 90 minutes
- **Downtime Component** - <!-- woodhouse: '{{ .Downtime }}'--> 30 minutes

{+Provide a brief summary indicating the affected zone+}

<!--e.g Restricting traffic to operating in two remaining zones due to a zonal outage in us-east1-d-->

## \[**For Gamedays only**\] Preparation Tasks

### One week before the gameday

1. [ ] Add an event to the [GitLab Production](https://calendar.google.com/calendar/embed?src=gitlab.com_si2ach70eb1j65cnu040m3alq0%40group.calendar.google.com) calendar.
1. [ ] Make an announcement on the [#f_gamedays](https://gitlab.enterprise.slack.com/archives/C07PV3F6J1W) Slack channel with this template:

   ```code
   Next week on [DATE & TIME] we will execute a CI Runners recovery game day. The process will involve emulating a single zone outage for the runners in `GPRD` to test our disaster recovery capabilities and measure if we are still within our RTO & RPO targets set by the  [DR working group](https://handbook.gitlab.com/handbook/company/working-groups/disaster-recovery/) for GitLab.com.
   See <https://gitlab.com/gitlab-com/gl-infra/production/-/issues/xxxxx>
   ```

   Then cross-post the message to the following channels:
   - [ ] [#g_production_engineering](https://gitlab.enterprise.slack.com/archives/C03QC5KNW5N)
   - [ ] [#test-platform](https://gitlab.enterprise.slack.com/archives/C3JJET4Q6)
   - [ ] [#staging](https://gitlab.enterprise.slack.com/archives/CLM200USV) (if applicable)
   - [ ] [#production](https://gitlab.enterprise.slack.com/archives/C101F3796) (if applicable)
1. [ ] Mention the release managers on the Slack announcement by mentioning `@release-managers` and await their approval.
1. [ ] Request approval from the Infrastructure manager, wait for approval and confirm by the ~manager_approved label.

### Just before the gameday begins

1. [ ] Before commencing the change, notify the EOC and release managers on _Slack_ with the following template and wait for their acknowledgement and approval

   ```code
   @release-managers or @sre-oncall LINK_TO_THIS_CR is scheduled for execution today at [TIME]. We will be emulating a single zone outage for CI Runners in `GPRD` to test our disaster recovery capabilities by increasing capacity to one of the zones and measure if we are still within our RTO & RPO targets. Kindly review and approve the CR
   ```

## Detailed steps for the change

### Change Steps - steps to take to execute the change

#### Execution

1. [ ] If you are conducting a practice (Gameday) run of this, consider starting a recording of the process now.
1. [ ] Note the start time in UTC in a comment to record this process duration.
1. [ ] Set label ~"change::in-progress" `/label ~change::in-progress`
1. [ ] Identify the currently active color and the current version. In this test, we will target the `private` shard which is serving internal traffic.
   - [ ] Open the [CI Runners Versions view](https://dashboards.gitlab.net/goto/fdX-HmZNR?orgId=1) and locate the 'instance' column to identify the active deployment colour for a shard and version. Record the information in the comment section of this issue.
   - [ ] Record the number of active [Runner Managers in the specific shard](https://dashboards.gitlab.net/goto/nr9YHmZHg?orgId=1)
1. [ ] Create an MR to set the wrong zone in **one of the blocks (dev/ops)** on the **active** color in the required shard (for ex. <https://gitlab.com/gitlab-com/gl-infra/chef-repo/-/blob/master/roles/runners-manager-private-blue-1.json#L107>). Merge the MR and wait for the CI to upload changes to the Chef Server. Updating this only will make `runners-manager-private-blue-1` lose its ability to create new ephemeral VMs, it will error out with an error, which will be logged in Kibana in [pubsub-runner-inf-gprd*](https://log.gprd.gitlab.net/app/discover#/?_g=h@06c5974&_a=h@5e1a06a) index. The runner-manager won't instantly lose its ability to process jobs, only when it attempts to recycle or scale ephemeral VMs will it start to error, additionally, it will lose its ability to process all jobs when all of the existing ephemeral VMs reach a point when they need to be recycled.
1. [ ] After the above MRs are merged and pipelines have completed, deploy the inactive colour for the shard using the ChatOps command in the `#production` Slack channel

   - [ ] `/runner run start {shard_name} {in_active_colour}`. Replace `shard_name` with the required shard and `colour` with the **inactive** colour (`blue` or `green`). The deployment job will start. e.g.

      ```code
      /runner run start private green
      ```

   - [ ] Verify the deployment at [CI Runners dashboard](https://dashboards.gitlab.net/goto/7HHqT5WNR?orgId=1) for a specific shard (choose a required shared in the dashboard)
1. [ ] Confirm that the new number of active [Runner Managers in the specific shard](https://dashboards.gitlab.net/goto/7HHqT5WNR?orgId=1) increased
1. [ ] Note the conclusion time in UTC in a comment to record this process duration.

#### Validation

Once traffic is restricted to our remaining two zones, let's identify the impact and look for problems.

- [ ] [CI Runners Deployment overview](https://dashboards.gitlab.net/goto/3ngM0cWNR?orgId=1)
- [ ] [CI Runner Versions view](https://dashboards.gitlab.net/goto/HICMA5ZHR?orgId=1)
- [ ] Confirm the jobs is successful after running the ChatOps command e.g.
  - [ ] [Example Successful ChatOps start](https://ops.gitlab.net/gitlab-com/gl-infra/ci-runners/deployer/-/jobs/15281738)
  - [ ] [Example Successful ChatOps stop](https://ops.gitlab.net/gitlab-com/gl-infra/ci-runners/deployer/-/jobs/15281796)
- [ ] Confirm at least some jobs have been provisioned in the new servers :point_right: [CI Runner Jobs](https://dashboards.gitlab.net/goto/lkc015WHR?orgId=1)

#### Wrapping up and cleanup

- [ ] Revert the MR to update the version on the inactive colour from the Execution steps above
- [ ] After the above MRs are merged and pipelines are completed and drain the previously changed colour for a shard using the `ChatOps` command in the `#production` Slack channel
  - [ ] `/runner run stop {shard_name} {same_colour}` i.e.

    ```code
    /runner run stop private green
    ```

  - [ ] verify the deployment at [CI Runners dashboard](https://dashboards.gitlab.net/goto/vHGv05ZNg?orgId=1) for a specific shard (choose a required shared in the dashboard)
- [ ] Confirm that the number of active [Runner Managers in the specific shard](https://dashboards.gitlab.net/goto/vHGv05ZNg?orgId=1) returned to pre-change value
- [ ] Set label ~"change::complete" `/label ~change::complete`
- [ ] Notify the `@release-managers` and `@sre-oncall` that the exercise is complete.
- [ ] Compile the real-time measurement of this process and update the [Recovery Measurements Runbook](https://gitlab.com/gitlab-com/runbooks/-/blob/master/docs/disaster-recovery/recovery-measurements.md?ref_type=heads).

## Rollback

### Rollback steps - steps to be taken in the event of a need to rollback this change

#### _It is estimated that this will take 5m to complete_

- [ ] Revert the MR to update the version on the inactive colour from the Execution steps above
- [ ] After the above MRs are merged and pipelines are completed and drain the previously changed colour for a shard using the `ChatOps` command in the `#production` Slack channel
  - [ ] `/runner run stop {shard_name} {same_colour}`. Replace `shard_name` with the required shard and `colour` with the inactive colour `(blue or green). The deployment job will start. i.e.

    ```bash
    /runner run stop private green
    ```

  - [ ] verify the deployment at [CI Runners dashboard](https://dashboards.gitlab.net/goto/vHGv05ZNg?orgId=1) for a specific shard (choose a required shared in the dashboard)
- [ ] Confirm that the number of active [Runner Managers in the specific shard](https://dashboards.gitlab.net/goto/vHGv05ZNg?orgId=1) returned to pre-change value
- [ ] Set label ~"change::complete" `/label ~change::aborted`
- [ ] Notify the `@release-managers` and `@sre-oncall` that the exercise has been aborted.

## Change Reviewer checklist

<!--To be filled out by the reviewer.-->

~C4 ~C3 ~C2 ~C1:

- [ ] Check if the following applies:
  - The **scheduled day and time** of execution of the change is appropriate.
  - The [change plan](#detailed-steps-for-the-change) is technically accurate.
  - The change plan includes **estimated timing values** based on previous testing.
  - The change plan includes a viable [rollback plan](#rollback).
  - The specified [metrics/monitoring dashboards](#key-metrics-to-observe) provide sufficient visibility for the change.

~C2 ~C1:

- [ ] Check if the following applies:
  - The complexity of the plan is appropriate for the corresponding risk of the change. (i.e. the plan contains clear details).
  - The change plan includes success measures for all steps/milestones during the execution.
  - The change adequately minimizes risk within the environment/service.
  - The performance implications of executing the change are well-understood and documented.
  - The specified metrics/monitoring dashboards provide sufficient visibility for the change.
    - If not, is it possible (or necessary) to make changes to observability platforms for added visibility?
  - The change has a primary and secondary SRE with knowledge of the details available during the change window.
  - The change window has been agreed upon with Release Managers in advance of the change. If the change is planned for APAC hours, this issue has an agreed pre-change approval.
  - The labels ~"blocks deployments" and/or ~"blocks feature-flags" are applied as necessary.

## Change Technician checklist

<!--To find out who is on-call, use the `@sre-oncall` handle in slack-->

- [ ] Check if all items below are complete:
  - The [change plan](#detailed-steps-for-the-change) is technically accurate.
  - This Change Issue is linked to the appropriate Issue and/or Epic
  - Change has been tested in staging and results are noted in a comment on this issue.
  - A dry-run has been conducted and results are noted in a comment on this issue.
  - The change execution window respects the [Production Change Lock periods](https://about.gitlab.com/handbook/engineering/infrastructure/change-management/#production-change-lock-pcl).
  - For ~C1 and ~C2 change issues, the change event is added to the [GitLab Production](https://calendar.google.com/calendar/embed?src=gitlab.com_si2ach70eb1j65cnu040m3alq0%40group.calendar.google.com) calendar.
  - For ~C1 and ~C2 change issues, the SRE on-call has been informed before the change is rolled out. (In the #production channel, mention `@sre-oncall` and this issue and await their acknowledgement.)
  - For ~C1 and ~C2 change issues, the SRE on-call provided approval with the ~eoc_approved label on the issue.
  - For ~C1 and ~C2 change issues, the Infrastructure Manager provided approval with the ~manager_approved label on the issue.
  - Release managers have been informed prior to any C1, C2, or ~"blocks deployments" change being rolled out. (In the #production channel, mention `@release-managers` and this issue and await their acknowledgement.)
  - There are currently no [active incidents](https://gitlab.com/gitlab-com/gl-infra/production/-/issues?scope=all&utf8=%E2%9C%93&state=opened&label_name%5B%5D=Incident%3A%3AActive) that are ~severity::1 or ~severity::2
  - If the change involves doing maintenance on a database host, an appropriate silence targeting the host(s) should be added for the duration of the change.

/label ~"C2"
/label ~"change::unscheduled"
/label ~"change"
/label ~"release-blocker"
/label ~"blocks deployments"
/label ~"Deploys-blocked-gstg"
/label ~"gamedays"
