# Production Change - Criticality 2 ~C2

## Change Summary

This production issue is to be used for Gamedays as well as recovery in case of a zonal outage. The objective of this test is to assess the resilience and recovery capability of our Redis infrastructure in the event of a failure. Redis plays a critical role in our system's performance and data availability, making it essential to ensure that the infrastructure is equipped to handle a variety of failure scenarios without significant downtime or data loss. The gameday exercise is designed to validate and strengthen disaster recovery strategies for Redis Sentinel and Redis Cluster setups by simulating real-world failure scenarios. The tests aim to ensure high availability, data integrity, and system resilience under failure conditions.

For this exercise:

- **Redis Cluster RateLimiting service** will be targeted due to its low-risk impact and high observability. This service stores transient rate-limiting counts with a short lifespan, making it an ideal candidate for disaster recovery validation.

### Gameday execution roles and details

| Role | Assignee |
|---|---|
| Change Technician | <!-- woodhouse: '`@{{ .Username }}`' --> |
| Change Reviewer | <!-- woodhouse: '@{{ .Reviewer }}' --> |

- **Services Impacted**  - ~"Service::RedisClusterRateLimiting"
- **Time tracking**      - <!-- woodhouse: '{{ .Duration}}' --> 90 minutes
- **Downtime Component** - <!-- woodhouse: '{{ .Downtime }}'--> 30 minutes

{+ Provide a brief summary indicating the affected zone +}
<!-- e.g Restricting traffic to operating in two remaining zones due to a zonal outage in us-east1-d -->

## [**For Gamedays only**] Preparation Tasks

1. [ ] One week before the gameday make an announcement on slack [production_engineering](https://gitlab.enterprise.slack.com/archives/C03QC5KNW5N) channel. Consider also posting this in the appropriate environment channels, [staging](https://gitlab.enterprise.slack.com/archives/CLM200USV) or [production](https://gitlab.enterprise.slack.com/archives/C101F3796).

    - **Example message:**

    ```code
    Next week on [DATE & TIME], we’ll be conducting a Redis Disaster Recovery (DR) test in the `gstg` environment. We’ll simulate node and zone failures to validate our failover process and ensure we’re meeting our RTO and RPO targets set by the  [DR working group](https://handbook.gitlab.com/handbook/company/working-groups/disaster-recovery/) for GitLab.com. See <https://gitlab.com/gitlab-com/gl-infra/production/-/issues/17274>
    ```

1. [ ] One week before the gameday notify the release managers on _Slack_ by mentioning `@release-managers` and referencing this issue and await their acknowledgment.
1. [ ] Add an event to the [GitLab Production](https://calendar.google.com/calendar/embed?src=gitlab.com_si2ach70eb1j65cnu040m3alq0%40group.calendar.google.com) calendar.
1. [ ] Post an FYI link of the slack message to the [#test-platform](https://gitlab.enterprise.slack.com/archives/C3JJET4Q6) channel on slack.
1. [ ] Before commencing the change notify the eoc on _Slack_ by mentioning `@sre-oncall` and referencing this issue and wait for approval by adding the ~eoc_approved label.
1. [ ] Before commencing the change notify the release managers on _Slack_ by mentioning `@release-managers` and referencing this issue and await their acknowledgment.

    - **Example message:**

    ```code
    @release-managers or @sre-oncall
    The [LINK_TO_THIS_CR] CR is scheduled for the Redis Disaster Recovery test. We’ll be simulating node and zone failures in `gstg` to validate failover and ensure we meet RTO & RPO targets. Please review and approve the CR.
    ```

## Detailed steps for the change

### Change Steps - steps to take to execute the change

#### Execution

1. [ ] If you are conducting a practice (Gameday) run of this, consider starting a recording of the process now.

1. [ ] Note the start time in UTC in a comment to record this process duration.

1. [ ] Set label ~"change::in-progress" `/label ~change::in-progress`

### Gameday Scenarios

### Pre-requisites for Verification

- [ ] Use any of these command to get the name of the Redis cluster node to login to

  ```bash
  # List all Redis instances
  gcloud compute instances list --project=gitlab-staging-1 | grep -i redis | grep ratelimiting

  # Alternative using knife in the `chef-repo`
  knife search node "name:*redis*" -a cloud.public_hostname -a cloud.provider_data.zone  | grep ratelimiting | grep staging
  ```

### Redis Cluster Primary Node Failure

- [ ] SSH to one of the redis nodes in the cluster

    ```bash
      ssh <node from prerequisite steps>
    ```

- [ ] To simulate the failure of a primary node in the cluster:
  - [ ] Identify the current primary nodes and their roles:

    ```bash
    sudo gitlab-redis-cli cluster nodes
    ```

    - Note the nodes marked as `master` and their corresponding replicas

  - [ ] Check cluster state before failover:

    ```bash
    sudo gitlab-redis-cli cluster info | head -n7
    sudo gitlab-redis-cli cluster slots
    ```

  - [ ] Stop one of the primary nodes:

    ```bash
    sudo systemctl stop redis-server
    ```

  - [ ] Monitor cluster state during failover:

    ```bash
    # Check cluster status
    sudo gitlab-redis-cli cluster info | head -n7

    # Check node roles and state
    sudo gitlab-redis-cli cluster nodes

    # Verify slot distribution
    sudo gitlab-redis-cli cluster slots
    ```

  - [ ] If cluster state becomes broken during failover:

    ```bash
    sudo gitlab-redis-cli --cluster fix 127.0.0.1:6379
    ```

  - [ ] Restart the failed primary:

    ```bash
    sudo systemctl start redis-server
    ```

  - [ ] Validate cluster recovery and reintegration:

    ```bash
    # Check cluster health
    sudo gitlab-redis-cli cluster info | head -n7

    # Verify node roles
    sudo gitlab-redis-cli cluster nodes

    # Confirm slot distribution
    sudo gitlab-redis-cli cluster slots

    # Expected cluster info output:
    # cluster_state:ok
    # cluster_slots_assigned:16384
    # cluster_slots_ok:16384
    # cluster_slots_pfail:0
    # cluster_slots_fail:0
    ```

### Zonal Failure Recovery for Redis Cluster

- [ ] SSH to one of the redis nodes in the cluster e.g. Ensure you replace the hostname with the one you got from the knife command earlier

    ```bash
      ssh <node from prerequisite steps>
    ```

- [ ] To simulate a zonal isolation for Redis:
  - [ ] Identify Redis instances in the target zone:

    ```bash
    gcloud compute instances list --project=gitlab-staging-1 --filter="name~redis AND zone:us-east1-b"
    ```

  - [ ] Before stopping instances, check cluster state:

    ```bash
    sudo gitlab-redis-cli cluster info | head -n7
    sudo gitlab-redis-cli cluster nodes
    ```

  - [ ] Isolate Redis nodes in the target zone:

    ```bash
    # Create firewall rules to block Redis cluster traffic (ports 6379, 16379) for specific instances
    gcloud compute firewall-rules create block-redis-cluster-traffic \
      --project=gitlab-staging-1 \
      --direction=INGRESS \
      --priority=1000 \
      --network=default \
      --target-tags=block-redis-cluster-traffic \
      --action=DENY \
      --rules=tcp:6379,tcp:16379

    # Apply the network tag to Redis instances in the target zone
    for instance in $(gcloud compute instances list --project=gitlab-staging-1 --filter="name~redis AND zone:us-east1-b" --format="value(name)"); do
      gcloud compute instances add-tags $instance --tags=block-redis-cluster-traffic --zone=us-east1-b
    done
    ```

  - [ ] Monitor cluster state during isolation:

    ```bash
    sudo gitlab-redis-cli cluster info | head -n7
    sudo gitlab-redis-cli cluster nodes
    ```

  - [ ] Verify slot coverage:

    ```bash
    sudo gitlab-redis-cli cluster slots
    ```

  - [ ] If cluster state becomes broken, fix it:

    ```bash
    sudo gitlab-redis-cli --cluster fix 127.0.0.1:6379
    ```

  - [ ] Restore connectivity:

    ```bash
    # Remove the firewall rule
    gcloud compute firewall-rules delete block-redis-cluster-traffic

    # Remove the network tags
    for instance in $(gcloud compute instances list --project=gitlab-staging-1 --filter="name~redis AND zone:us-east1-b" --format="value(name)"); do
      gcloud compute instances remove-tags $instance --tags=block-redis-cluster-traffic --zone=us-east1-b
    done
    ```

  - [ ] Verify cluster recovery:

    ```bash
    sudo gitlab-redis-cli cluster info | head -n7
    sudo gitlab-redis-cli cluster nodes
    sudo gitlab-redis-cli cluster slots
    ```

    > **Warning**: Resetting a Redis cluster node may lead to data loss if the node contains unreplicated writes.
    ---
    > **Note**  
    > Incase a split-brain scenario occurs though rare (which is where 2 nodes believe they are leaders) these command can help force the incorrect master to step down

      ```bash
        # First identify the nodes 
        sudo gitlab-redis-cli cluster nodes | grep master

        # Verify the Cluster's Slot Ownership to understand which nodes own which slot ranges. Ensure there's no overlap or inconsistency.
        sudo gitlab-redis-cli cluster slots

        #Quorum and Node Status: Check the cluster's overall health and ensure there is a quorum
        
        sudo gitlab-redis-cli cluster info

        # Isolate and Resolve:  Stop the "incorrect" master to isolate it temporarily. Verify the state of the remaining nodes and confirm the majority view.

        # Force the incorrect master to step down and become a replica
        sudo gitlab-redis-cli cluster reset soft
        sudo gitlab-redis-cli cluster replicate <correct_master_id>
        ```
    ---

### Redis Cluster Multi-Node Failure

- [ ] SSH to any two of the redis nodes across different shards in the cluster e.g. Ensure you replace the hostname with the one you got from the knife command earlier

    ```bash
      ssh <node from prerequisite steps>
    ```

- [ ] Simulate the failure of multiple nodes across different shards:
  - [ ] Stop nodes:

    ```bash
    # Check Redis service status and stop the redis server
    sudo systemctl status redis-server
    sudo systemctl stop redis-server
    ```

  - [ ] Identify shard topology and current primary nodes:

    ```bash
    sudo gitlab-redis-cli cluster nodes
    ```

    - Note the `master` nodes and their corresponding replicas.

  - [ ] Monitor cluster state:

    ```bash
    sudo gitlab-redis-cli cluster info | head -n7

    # Expected output should show:
    # cluster_state:ok
    # cluster_slots_assigned:16384
    # cluster_slots_ok:16384
    # cluster_slots_pfail:0
    # cluster_slots_fail:0
    ```

  - [ ] Verify slot distribution:

    ```bash
    sudo gitlab-redis-cli cluster slots
    ```

  - [ ] If cluster state becomes broken during testing, fix it using:

    ```bash
    sudo gitlab-redis-cli --cluster fix 127.0.0.1:6379
    ```

  - [ ] Restart nodes:

    ```bash
    sudo systemctl start redis-server
    ```

  - [ ] Verify cluster recovery:

    ```bash
    sudo gitlab-redis-cli cluster info | head -n7
    sudo gitlab-redis-cli cluster nodes
    ```

### Redis Backup and Restore Validation for Cluster

- [ ] SSH to one of the redis nodes in the cluster e.g. Ensure you replace the hostname with the one you got from the knife command earlier

    ```bash
      ssh <node from prerequisite steps>
    ```

- [ ] Test the ability to recover from a catastrophic failure:
  - [ ] Backup Redis data:

    ```bash
    sudo gitlab-redis-cli bgsave
    sudo cp /var/opt/gitlab/redis/dump.rdb /backup/path/dump.rdb
    ```

  - [ ] Simulate failure:

    ```bash
    sudo systemctl stop redis-server
    ```

  - [ ] Restore data:

    ```bash
    sudo cp /backup/path/dump.rdb /var/opt/gitlab/redis/dump.rdb
    sudo systemctl start redis-server
    ```

  - [ ] Verify cluster state after restore:

    ```bash
    sudo gitlab-redis-cli cluster info | head -n7
    sudo gitlab-redis-cli cluster nodes
    ```

  - [ ] Check slot distribution:

    ```bash
    sudo gitlab-redis-cli cluster slots
    ```

### Data Persistence Validation

- [ ] SSH to one of the redis nodes in the cluster e.g. Ensure you replace the hostname with the one you got from the knife command earlier

    ```bash
      ssh <node from prerequisite steps>
    ```

- [ ] Verify that data persists across failures:
  - [ ] Insert test data:

    ```bash
    sudo gitlab-redis-cli
    > SET test_key "test_value"
    > SAVE
    ```

  - [ ] Simulate failure:

    ```bash
    sudo systemctl stop redis-server
    ```

  - [ ] Restart Redis:

    ```bash
    sudo systemctl start redis-server
    ```

  - [ ] Validate persistence:

    ```bash
    sudo gitlab-redis-cli
    > GET test_key
    ```

  - [ ] Verify cluster state after restart:

    ```bash
    sudo gitlab-redis-cli cluster info | head -n7
    sudo gitlab-redis-cli cluster nodes
    ```

- [ ] Note the conclusion time in UTC in a comment to record this process duration.

#### Validation Metrics

- [ ] SSH to one of the redis nodes in the cluster or you can confirm from the [Redis cluster data dashboard](https://dashboards.gitlab.net/goto/5CuEC4ONR?orgId=1) e.g. Ensure you replace the hostname with the one you got from the knife command earlier

    ```bash
      ssh <node from prerequisite steps>
    ```

- [ ] Monitor these specific metrics during the test:
  - Redis Connection Status:

    ```bash
    sudo gitlab-redis-cli PING
    ```

  - Replication Status:

    ```bash
    sudo gitlab-redis-cli cluster info
    sudo gitlab-redis-cli cluster nodes
    ```

  - Client Connections:

    ```bash
    sudo gitlab-redis-cli CLIENT LIST
    ```

  - Slot Distribution:

    ```bash
    sudo gitlab-redis-cli cluster slots
    ```

  - Cluster Health:

    ```bash
    # Check cluster state
    sudo gitlab-redis-cli cluster info | head -n7

    # Expected output should show:
    # cluster_state:ok
    # cluster_slots_assigned:16384
    # cluster_slots_ok:16384
    # cluster_slots_pfail:0
    # cluster_slots_fail:0
    ```

- [ ] Verify these metrics in Prometheus/Grafana:
  - [ ] Redis operation latency
  - [ ] Connection success rate
  - [ ] Error rates

## Dashboards and Monitoring

### Redis Overview Dashboards

- **Cache**: [Cache Overview Dashboard](https://dashboards.gitlab.net/d/redis-cluster-cache-main/redis-cluster-cache-overview)
- **Persistent**: [Persistent Dashboard](https://dashboards.gitlab.net/d/redis-main/redis-overview)
- **Sidekiq**: [Sidekiq Dashboard](https://dashboards.gitlab.net/d/redis-sidekiq-main/redis-sidekiq-overview)
- **RateLimiting**: [RateLimiting Dashboard](https://dashboards.gitlab.net/d/redis-cluster-ratelimiting-main/redis-cluster-ratelimiting-overview)
- **Sessions**: [Sessions Dashboard](https://dashboards.gitlab.net/d/redis-sessions-main/redis-sessions-overview)

#### Key Metrics Thresholds

- Redis Primary CPU:
  - Normal: < 60%
  - Warning: 60-75%
  - Critical: > 75%
- Replication Lag:
  - Normal: < 500ms
  - Warning: 500ms-2s
  - Critical: > 2s

### Wrapping up and cleanup

- [ ] Set label ~"change::complete" `/label ~change::complete`
- [ ] Notify the `@release-managers` and `@sre-oncall` that the exercise is complete.
- [ ] Compile the real time measurement of this process and update the [Recovery Measrements Runbook](https://gitlab.com/gitlab-com/runbooks/-/blob/master/docs/disaster-recovery/recovery-measurements.md?ref_type=heads).

## Rollback

### Rollback steps - steps to be taken in the event of a need to rollback this change

#### _It is estimated that this will take 5-10m to complete_

1. [ ] SSH to one of the redis nodes in the cluster e.g. Ensure you replace the hostname with the one you got from the knife command earlier

    ```bash
      ssh <node from prerequisite steps>
    ```

1. [ ] For Redis-specific firewall rules rollback:

   ```bash
   # Remove the firewall rule
   gcloud compute firewall-rules delete block-redis-cluster-traffic
   
   # Remove the network tags from instances in the target zone
   for instance in $(gcloud compute instances list --project=gitlab-staging-1 --filter="name~redis-cluster-ratelimiting AND zone:us-east1-b" --format="value(name)"); do
     gcloud compute instances remove-tags $instance --tags=block-redis-cluster-traffic --zone=us-east1-b
   done
1. [ ] For Redis Cluster RateLimiting Recovery:

    ```bash
      # Check cluster status and health
      sudo gitlab-redis-cli cluster info | head -n7

      # Check cluster nodes and their roles
      sudo gitlab-redis-cli cluster nodes

      # Verify slot distribution is complete (16384 slots)
      sudo gitlab-redis-cli cluster slots
     ```
  
1. [ ] Fix cluster if needed:  

     ```bash
        #    Check cluster health
      sudo gitlab-redis-cli --cluster check 127.0.0.1:6379

      # Fix cluster if issues are found
      sudo gitlab-redis-cli --cluster fix 127.0.0.1:6379
     ```

1. [ ]  Validate connectivity across cluster:

     ```bash
          # Check connection to each Redis instance
      for instance in $(gcloud compute instances list --project=gitlab-staging-1 --filter="name~redis" --format="value(name,networkInterfaces[0].networkIP)"); do
        echo "Testing $instance"
        sudo gitlab-redis-cli -h $(echo $instance | cut -d' ' -f2) ping
      done
     ```

1. [ ]  Additional health checks:

      ```bash
          # Check slowlog for unusual entries
        sudo gitlab-redis-cli slowlog get 10

        # Check if all slots are covered
        sudo gitlab-redis-cli cluster slots
        ```
1. Verify metrics in RateLimiting Dashboard:

   - [ ] Redis operation latency has returned to normal (< 60% CPU)
   - [ ] Connection success rate is 100%
   - [ ] Error rates have returned to baseline
   - [ ] All slots are properly assigned
   - [ ]  Cluster state is 'ok'

1. [ ] Post-Rollback Tasks:

   - [ ] Update the change issue with rollback completion status
   - [ ] Notify `@release-managers` and `@sre-oncall` of rollback completion
   - [ ] Document any issues encountered during rollback in the change issue
   - [ ] Update labels:

     ```code
     /label ~"change::rolled-back"
     ```

## Change Reviewer checklist

<!--
To be filled out by the reviewer.
-->
~C4 ~C3 ~C2 ~C1:

- [ ] Check if the following applies:
  - The **scheduled day and time** of execution of the change is appropriate.
  - The [change plan](#detailed-steps-for-the-change) is technically accurate.
  - The change plan includes **estimated timing values** based on previous testing.
  - The change plan includes a viable [rollback plan](#rollback).
  - The specified [metrics/monitoring dashboards](#key-metrics-to-observe) provide sufficient visibility for the change.

~C2 ~C1:

- [ ] Check if the following applies:
  - The complexity of the plan is appropriate for the corresponding risk of the change. (i.e. the plan contains clear details).
  - The change plan includes success measures for all steps/milestones during the execution.
  - The change adequately minimizes risk within the environment/service.
  - The performance implications of executing the change are well-understood and documented.
  - The specified metrics/monitoring dashboards provide sufficient visibility for the change.
    - If not, is it possible (or necessary) to make changes to observability platforms for added visibility?
  - The change has a primary and secondary SRE with knowledge of the details available during the change window.
  - The change window has been agreed upon with Release Managers in advance of the change. If the change is planned for APAC hours, this issue has an agreed pre-change approval.
  - The labels ~"blocks deployments" and/or ~"blocks feature-flags" are applied as necessary.

## Change Technician checklist

<!--
To find out who is on-call, use the `@sre-oncall` handle in slack
-->

- [ ] Check if all items below are complete:
  - The [change plan](#detailed-steps-for-the-change) is technically accurate.
  - This Change Issue is linked to the appropriate Issue and/or Epic
  - Change has been tested in staging and results are noted in a comment on this issue.
  - A dry-run has been conducted and results are noted in a comment on this issue.
  - The change execution window respects the [Production Change Lock periods](https://about.gitlab.com/handbook/engineering/infrastructure/change-management/#production-change-lock-pcl).
  - For ~C1 and ~C2 change issues, the change event is added to the [GitLab Production](https://calendar.google.com/calendar/embed?src=gitlab.com_si2ach70eb1j65cnu040m3alq0%40group.calendar.google.com) calendar.
  - For ~C1 and ~C2 change issues, the SRE on-call has been informed before the change is rolled out. (In the #production channel, mention `@sre-oncall` and this issue and await their acknowledgement.)
  - For ~C1 and ~C2 change issues, the SRE on-call provided approval with the ~eoc_approved label on the issue.
  - For ~C1 and ~C2 change issues, the Infrastructure Manager provided approval with the ~manager_approved label on the issue.
  - Release managers have been informed prior to any C1, C2, or ~"blocks deployments" change being rolled out. (In the #production channel, mention `@release-managers` and this issue and await their acknowledgement.)
  - There are currently no [active incidents](https://gitlab.com/gitlab-com/gl-infra/production/-/issues?scope=all&utf8=%E2%9C%93&state=opened&label_name[]=Incident%3A%3AActive) that are ~severity::1 or ~severity::2
  - If the change involves doing maintenance on a database host, an appropriate silence targeting the host(s) should be added for the duration of the change.

/label ~"C2"
/label ~"change::unscheduled"
/label ~"change"
/label ~"release-blocker"
/label ~"blocks deployments"
/label ~"Deploys-blocked-gstg"
/label ~"gamedays"
