#! /usr/bin/env bash

set -euo pipefail

set -x

dry_run="${DRY_RUN:-1}"

script_installation_path='/var/opt/gitlab/patroni/scripts/drop-database.sql'

cp /tmp/production/src/gl-infra-6071-create-database/drop-database.sql "${script_installation_path}"
chown gitlab-psql:gitlab-psql "${script_installation_path}"

if [ "${dry_run}" == '0' ]; then
    echo 'Dropping roles:'
    gitlab-psql --file="${script_installation_path}"
    rm "${script_installation_path}"
else
    echo "[Dry-run] Would have ran command: gitlab-psql --file=${script_installation_path}"
fi
