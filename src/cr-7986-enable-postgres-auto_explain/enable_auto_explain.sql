-- Enable auto_explain for 5% of queries with a 1-second logging threshold and full details except log_timing off/false.
ALTER SYSTEM SET auto_explain.sample_rate = 0.05 ;
ALTER SYSTEM SET auto_explain.log_min_duration = '1s' ;
ALTER SYSTEM SET auto_explain.log_analyze = true ;
ALTER SYSTEM SET auto_explain.log_timing = false ;
ALTER SYSTEM SET auto_explain.log_buffers = true ;
ALTER SYSTEM SET auto_explain.log_settings = true ;
